package auto.estudo;

public class TesteDeImposto {

	public static void main(String[] args) {
		Imposto ikcv = new IKCV();
		Imposto icpp = new ICPP();
		
		Orcamento orcamento = new Orcamento(450.00);
		orcamento.adicionaItem(new Item("Mochila", 150));
		orcamento.adicionaItem(new Item("Mochila", 20));
		
		CalculadorDeImposto calculadorDeImposto = new CalculadorDeImposto();
		calculadorDeImposto.realizaCalculo(orcamento, ikcv);
		calculadorDeImposto.realizaCalculo(orcamento, icpp);
	}

}
