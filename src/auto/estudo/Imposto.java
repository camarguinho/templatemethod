package auto.estudo;

public interface Imposto {

	double calcula(Orcamento orcamento);
	
}
