TEMPLATE METHOD: auxilia na definição de um algoritmo com partes do mesmo definidos por métodos abstratos. As subclasses devem se responsabilizar por estas partes abstratas, deste algoritmo, que serão implementadas, possivelmente de várias formas. Ou seja, cada subclasse irá implementar a sua necessidade e oferecer um comportamento concreto construindo todo o algoritmo.

Para testar o padrão, execute a classe TesteDeImposto.
